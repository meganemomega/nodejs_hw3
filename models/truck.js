const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model(
    'truck',
    new Schema(
        {
          created_by: {
            required: true,
            type: String,
          },
          assigned_to: {
            type: String,
          },
          type: {
            type: String,
            enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
            required: true,
          },
          status: {
            type: String,
            enum: ['OL', 'IS'],
          },
          created_date: {
            required: true,
            type: Date,
          },
        },
        {versionKey: false},
    ),
);
