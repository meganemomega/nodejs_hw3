const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;

module.exports = mongoose.model(
    'user',
    new Schema(
        {
          email: {
            required: true,
            type: String,
            unique: true,
            validate: (val) => {
              return validator.isEmail(val);
            },
          },
          role: {
            required: true,
            type: String,
            enum: ['SHIPPER', 'DRIVER'],
          },
          created_date: {
            required: true,
            type: Date,
          },
        },
        {versionKey: false},
    ),
);
