const mongoose = require('mongoose');

module.exports = mongoose.model('registrationCredential', {
  email: {
    required: true,
    type: String,
    unique: true,
  },
  password: {
    required: true,
    type: String,
  },
  role: {
    required: true,
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
  },
});
