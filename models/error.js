const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model(
    'error',
    new Schema(
        {
          message: {
            required: true,
            type: String,
          },
        },
        {_id: false},
    ),
);
