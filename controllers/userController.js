const User = require('../models/user');
const Truck = require('../models/truck');
const Credential = require('../models/credential');
const CustomError = require('../models/error');

module.exports.getOwnUser = (request, response) => {
  User.findOne({_id: request.user._id})
      .exec()
      .then((myUser) => {
        if (!myUser) {
          return response
              .status(400)
              .json(new CustomError({message: 'Own user not found'}));
        }
        response.status(200).json({user: myUser});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.deleteOwnUser = (request, response) => {
  User.findOne({_id: request.user._id})
      .exec()
      .then((myUser) => {
        if (!myUser) {
          return response
              .status(400)
              .json(new CustomError({message: 'Own user not found'}));
        }
        User.deleteOne({_id: request.user._id}, function(err) {
        // question with 400/500 code
          if (err) {
            response.status(500).json(new CustomError({message: err.message}));
          }
          Credential.deleteOne({_id: request.user._id}, function(err) {
            if (err) {
              response
                  .status(500).json(new CustomError({message: err.message}));
            }

            Truck.deleteMany({userId: request.user._id})
                .exec()
                .then(() => {
                  response.status(200).json({message: 'Success'});
                })
                .catch((err) => {
                  response
                      .status(500)
                      .json(new CustomError({message: err.message}));
                });
          });
        });
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.changePasswordOfOwnUser = (request, response) => {
  const {oldPassword, newPassword} = request.body;
  if ( !oldPassword || !newPassword) {
    return response
        .status(400)
        .json(new CustomError(
            {message: 'No old password or new password provided.'},
        ));
  }

  Credential.findOne({_id: request.user._id})
      .exec()
      .then((myUser) => {
        if (!myUser) {
          return response
              .status(400)
              .json(new CustomError({message: 'Own user not found'}));
        }
        if (oldPassword !== myUser.password) {
          return response
              .status(400)
              .json(new CustomError({message: 'Wrong old password'}));
        }
        myUser.password = newPassword;
        myUser.save();
        response.status(200).json({message: 'Password changed successfully'});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};
