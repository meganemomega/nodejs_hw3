/* eslint-disable camelcase */
/* eslint-disable require-jsdoc */
const Truck = require('../models/truck');
const CustomError = require('../models/error');

module.exports.getTrucks = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  Truck.find({created_by: request.user._id}, function(err, trucksList) {
    if (!trucksList) {
      return response
          .status(400)
          .json(new CustomError({message: 'No trucks of this user found'}));
    }
    response.status(200).json([{trucks: trucksList}]);
  }).catch((err) => {
    response.status(500).json(new CustomError({message: err.message}));
  });
};

module.exports.getTruckById = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  const id = request.params.id;

  Truck.findOne({_id: id, created_by: request.user._id})
      .exec()
      .then((truckById) => {
        if (!truckById) {
          return response.status(400).json(
              new CustomError({
                message: 'No truck with such Id found',
              }),
          );
        }
        response.status(200).json({truck: truckById});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.updateTruckById = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  const id = request.params.id;
  const {type} = request.body;

  Truck.findOne({_id: id, created_by: request.user._id})
      .exec()
      .then((truckById) => {
        if (!truckById) {
          return response.status(400).json(
              new CustomError({
                message: 'No truck with such Id found',
              }),
          );
        }
        updateTruck(
            id,
            response,
            {
              type: type,
            },
            'Truck details changed successfully',
        );
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.assignTruckById = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  const id = request.params.id;

  Truck.findOne({_id: id, created_by: request.user._id})
      .exec()
      .then((truckById) => {
        if (!truckById) {
          return response.status(400).json(
              new CustomError({
                message: 'No truck with such Id found',
              }),
          );
        }
        Truck.updateMany(
            {created_by: request.user._id},
            {assigned_to: null, status: null},
            {runValidators: true},
            (err) => {
              if (err) {
                return response
                    .status(500)
                    .json(new CustomError({message: err.message}));
              }
              updateTruck(
                  id,
                  response,
                  {
                    assigned_to: request.user._id,
                    status: 'IS',
                  },
                  'Truck assigned successfully',
              );
            },
        );
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

function updateTruck(truckId, response, transform, successMessage) {
  Truck.findOneAndUpdate(
      {_id: truckId},
      transform,
      {runValidators: true},
      (err) => {
        if (err) {
          return response
              .status(500)
              .json(new CustomError({message: err.message}));
        }
        response.status(200).json({message: successMessage});
      },
  );
}

module.exports.deleteTruckById = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  const id = request.params.id;

  Truck.findOne({_id: id, created_by: request.user._id})
      .exec()
      .then((truckById) => {
        if (!truckById) {
          return response.status(400).json(
              new CustomError({
                message: 'No truck with such Id found',
              }),
          );
        }
        Truck.deleteOne({_id: id}, function(err) {
          if (err) {
            response.status(500).json(new CustomError({message: err.message}));
          }
          response.status(200).json({message: 'Truck deleted successfully'});
        });
      });
};

module.exports.addTruck = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only driver can update load.'}));
  }
  const {type} = request.body;
  // if (!type) {
  //   return response
  //       .status(400)
  //       .json(new CustomError({message: 'No type passed'}));
  // }
  const created_date = new Date();
  const userId = request.user._id;

  const truck = new Truck({
    created_by: userId,
    type: type,
    created_date: created_date,
  });
  truck
      .save()
      .then(() => {
        response.status(200).json({message: 'Truck created successfully'});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};
