/* eslint-disable camelcase */
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const Credential = require('../models/credential');

const {secret} = require('../config/auth');
const CustomError = require('../models/error');
const saltRounds = 10;

module.exports.register = (request, response) => {
  const {email, password, role} = request.body;
  const created_date = new Date();
  // if (!role) {
  //   return response
  //       .status(400)
  //       .json(new CustomError({message: 'No user role provided.'}));
  // }
  // if (role !== 'DRIVER' && role !== 'SHIPPER') {
  //   return response
  //       .status(400)
  //       .json(new CustomError({message: 'Unknown user role.'}));
  // }

  const credential = new Credential(
      {email, password: bcrypt.hashSync(password, saltRounds)},
  );
  credential
      .save()
      .then(() => {
        const user = new User({
          _id: credential._id,
          email,
          role,
          created_date,
        });
        user
            .save()
            .then(() => {
              response
                  .status(200).json({message: 'Profile created successfully'});
            })
            .catch((err) => {
              response
                  .status(500).json(new CustomError({message: err.message}));
            });
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.login = (request, response) => {
  const {email, password} = request.body;

  Credential.findOne({email})
      .exec()
      .then((credential) => {
        if (!credential) {
          return response.status(400).json(
              new CustomError({
                message: 'No user with such email found.',
              }),
          );
        }
        if (!bcrypt.compareSync(password, credential.password)) {
          return response.status(400).json(
              new CustomError({
                message: 'Wrong password.',
              }),
          );
        }
        User.findOne({_id: credential._id})
            .exec()
            .then((user) => {
              if (!user) {
                return response.status(400).json(
                    new CustomError({
                      message: 'No user with such email and password found',
                    }),
                );
              }
              response.status(200).json({
                jwt_token: jwt.sign(JSON.stringify(user), secret),
              });
            })
            .catch((err) => {
              response
                  .status(500).json(new CustomError({message: err.message}));
            });
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};
