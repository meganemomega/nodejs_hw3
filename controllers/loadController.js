/* eslint-disable guard-for-in */
/* eslint-disable require-jsdoc */
/* eslint-disable camelcase */
const Load = require('../models/load');
const CustomError = require('../models/error');
const Truck = require('../models/truck');
const truckTypes = {
  'SPRINTER': [300, 250, 170, 1700],
  'SMALL STRAIGHT': [500, 250, 170, 2500],
  'LARGE STRAIGHT': [700, 350, 200, 4000],
};

module.exports.getLoads = (request, response) => {
  Load.find(
      {
        $or: [{created_by: request.user._id}, {assigned_to: request.user._id}],
      },
      function(err, loadsList) {
        if (!loadsList) {
          return response.status(400).json(
              new CustomError({
                message: 'No loads of this user found',
              }),
          );
        }
        return response.status(200).json([{loads: loadsList}]);
      },
  ).catch((err) => {
    response.status(500).json(new CustomError({message: err.message}));
  });
};

module.exports.addLoad = (request, response) => {
  if (request.user.role !== 'SHIPPER') {
    return response
        .status(400)
        .json(new CustomError({message: 'Only shipper can create a load.'}));
  }
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = request.body;
  // [name, payload, pickup_address, delivery_address, dimensions].forEach(
  //     // validate input fields
  //     (element) => {
  //       if (!element) {
  //         return response
  //             .status(400)
  //             .json(new CustomError({message: `No ${element} passed`}));
  //       }
  //     },
  // );
  const created_date = new Date();
  const created_by = request.user._id;
  const logs = []; // creating logs
  const time = new Date();
  logs.push({
    message: 'Load created successfully',
    time,
  });

  const load = new Load({
    created_by,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_date,
    logs,
  });
  load
      .save()
      .then(() => {
        response.status(200).json({message: 'Load created successfully'});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.getDriverActiveLoad = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response.status(400).json(
        new CustomError({
          message: 'Only driver can view active loads.',
        }),
    );
  }
  Load.find(
      {
        assigned_to: request.user._id,
        status: 'ASSIGNED', // active load
      },
      function(err, loadById) {
        if (!loadById) {
          return response.status(400).json(
              new CustomError({
                message: 'No active loads of this driver found',
              }),
          );
        }
        return response.status(200).json({load: loadById});
      },
  ).catch((err) => {
    response.status(500).json(new CustomError({message: err.message}));
  });
};

module.exports.iterateStateOfLoad = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response.status(400).json(
        new CustomError({
          message: 'Only driver can iterate load state.',
        }),
    );
  }
  Load.findOne({assigned_to: request.user._id, status: 'ASSIGNED'})
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json(
              new CustomError({
                message: 'No active load with such Id found',
              }),
          );
        }
        // get enum state values
        const stateEnum = load.schema.path('state').enumValues;
        const nextIndex = stateEnum.indexOf(load.state) + 1;
        if (nextIndex > 3) {
          return response.status(400).json(
              new CustomError({
                message: 'Load already shipped',
              }),
          );
        }
        const nextState = stateEnum[nextIndex];

        const logs = load.logs; // creating logs
        const time = new Date();

        if (nextState === 'Arrived to delivery') {
          Truck.findOneAndUpdate(
              {assigned_to: load.assigned_to, status: 'OL'},
              {status: 'IS'},
              (err) => {
                if (err) {
                  return response
                      .status(500)
                      .json(new CustomError({message: err.message}));
                }
                logs.push({
                  message: 'Load was shipped',
                  time,
                });
                updateLoad(
                    load.id,
                    response,
                    {
                      state: nextState,
                      status: 'SHIPPED',
                      logs,
                    },
                    {message: 'Load was shipped'},
                );
              },
          );
        } else {
          logs.push({
            message: `Load state changed to '${nextState}'`,
            time,
          });

          updateLoad(
              load.id,
              response,
              {
                state: nextState,
                logs,
              },
              {message: `Load state changed to '${nextState}'`},
          );
        }
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.getLoadById = (request, response) => {
  const id = request.params.id;

  Load.findOne({
    _id: id,
    $or: [{created_by: request.user._id}, {assigned_to: request.user._id}],
  })
      .exec()
      .then((loadById) => {
        if (!loadById) {
          return response.status(400).json(
              new CustomError({
                message: 'No load with such Id found',
              }),
          );
        }
        response.status(200).json({load: loadById});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

module.exports.updateLoadById = (request, response) => {
  if (request.user.role !== 'SHIPPER') {
    return response.status(400).json(
        new CustomError({
          message: 'Only shipper can update load.',
        }),
    );
  }
  const id = request.params.id;
  const updateValues = {};
  if (request.body.name) updateValues.name = request.body.name;
  if (request.body.payload) updateValues.payload = request.body.payload;
  if (request.body.pickup_address) {
    updateValues.pickup_address = request.body.pickup_address;
  }
  if (request.body.delivery_address) {
    updateValues.delivery_address = request.body.delivery_address;
  }
  if (
    request.body.dimensions &&
    request.body.dimensions.width &&
    request.body.dimensions.height &&
    request.body.dimensions.length
  ) {
    updateValues.dimensions = request.body.dimensions;
  }

  Load.findOne({
    _id: id,
    $or: [{created_by: request.user._id}, {assigned_to: request.user._id}],
  })
      .exec()
      .then((loadById) => {
        if (!loadById) {
          return response.status(400).json(
              new CustomError({
                message: 'No load with such Id found',
              }),
          );
        }
        if (loadById.status !== 'NEW') {
          return response.status(400).json(
              new CustomError({
                message: 'Can\'t update active load',
              }),
          );
        }
        updateLoad(id, response, updateValues, {
          message: 'Load details changed successfully',
        });
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};

async function updateLoad(loadId, response, transform, successMessage) {
  Load.findOneAndUpdate({_id: loadId}, transform, (err) => {
    if (err) {
      response.status(500).json(new CustomError({message: err.message}));
    }
    response.status(200).json(successMessage);
  });
}

module.exports.deleteLoadById = (request, response) => {
  if (request.user.role !== 'SHIPPER') {
    return response.status(400).json(
        new CustomError({
          message: 'Only shipper can delete load.',
        }),
    );
  }
  const id = request.params.id;

  Load.findOne({
    _id: id,
    $or: [{created_by: request.user._id}, {assigned_to: request.user._id}],
  })
      .exec()
      .then((loadById) => {
        if (!loadById) {
          return response.status(400).json(
              new CustomError({
                message: 'No load with such Id found',
              }),
          );
        }
        if (loadById.status !== 'NEW') {
          return response.status(400).json(
              new CustomError({
                message: 'Can\'t delete active load',
              }),
          );
        }

        Load.deleteOne({_id: id}, function(err) {
          if (err) {
            response.status(500).json(new CustomError({message: err.message}));
          }
          response.status(200).json({message: 'Load deleted successfully'});
        });
      });
};

// module.exports.postLoadById = (request, response) => {  //promise function
//     if (request.user.role !== "SHIPPER") {
//         return response.status(400).json(
//             new CustomError({
//                 message: "Only shipper can post its load.",
//             })
//         );
//     }
//     const id = request.params.id;
//     Load.findOne({
//         _id: id,
//         created_by: request.user._id,
//     })
//         .exec()
//         .then((load) => {
//             if (!load) {
//                 return response.status(400).json(
//                     new CustomError({
//                         message: "No load with such Id found",
//                     })
//                 );
//             }
//             if (load.status !== "NEW") {
//                 return response.status(400).json(
//                     new CustomError({
//                         message: "Load was already posted or shipped",
//                     })
//                 );
//             }

//             const logs = load.logs; //creating logs
//             const time = new Date();
//             logs.push({
//                 message: `Load was posted`,
//                 time,
//             });

//             Load.findOneAndUpdate(
//             { _id: load._id },
//             { status: "POSTED", logs },
//             (err) => {
//                 if (err) {
//                     return response
//                         .status(500)
//                         .json(new CustomError({ message: err.message }));
//                 }

//                 let neededTypeOfTruck = "";
//        //const loadVolume = load.dimensions.reduce((acc, val) => acc * val);

//                 for (const i in truckTypes) {
//        //let truckVolume = truckTypes[i].reduce((acc, val) => acc * val);
//                     const [width, length, height, payload] = truckTypes[i];
//                     if (
//                         load.payload <= payload &&
//                         ((load.dimensions.width <= width &&
//                             load.dimensions.length <= length &&
//              load.dimensions.height <= height) || //width changes with length
//                             (load.dimensions.width <= length &&
//                                 load.dimensions.length <= width &&
//                                 load.dimensions.height <= height))
//                     ) {
//                         neededTypeOfTruck = i;
//                         break;
//                     }
//                 }

//                 if (!neededTypeOfTruck) {
//                     updateLoad(
//                         load._id,
//                         response,
//                         {
//                             status: "NEW",
//                         },
//                         {
//                             message: "Load wasn't posted.",
//                             driver_found: false,
//                         }
//                     );
//                     return;
//                 }
//                 Truck.findOne({
//                     type: neededTypeOfTruck,
//                     status: "IS",
//                     assigned_to: { $ne: null },
//                 })
//                     .exec()
//                     .then((truckById) => {
//                         if (!truckById) {
//                             updateLoad(
//                                 load._id,
//                                 response,
//                                 {
//                                     status: "NEW",
//                                 },
//                                 {
//                                     message: "Load wasn't posted.",
//                                     driver_found: false,
//                                 }
//                             );
//                             return;
//                         }
//                         Truck.findOneAndUpdate(
//                         { _id: truckById._id },
//                         { status: "OL" },
//                         (err) => {
//                             if (err) {
//                                 return response.status(500).json(
//                                     new CustomError({
//                                         message: err.message,
//                                     })
//                                 );
//                             }
//                             updateLoad(
//                                 load._id,
//                                 response,
//                                 {
//                                     assigned_to: truckById.assigned_to,
//                                     status: "ASSIGNED",
//                                     state: "En route to Pick Up",
//                                 },
//                                 {
//                                     message: "Load posted successfully",
//                                     driver_found: true,
//                                 }
//                             );
//                         }
//                         );
//                     })
//                     .catch((err) => {
//                         response
//                             .status(500)
//                             .json(
//                                 new CustomError({ message: err.message })
//                             );
//                     });
//                 }
//             );
//         })
//         .catch((err) => {
//             response
//                 .status(500)
//                 .json(new CustomError({ message: err.message }));
//         });
// };

module.exports.postLoadById = async (request, response) => {
  // async/await func
  try {
    const id = request.params.id;
    const load = await Load.findOne({
      _id: id,
      created_by: request.user._id,
      status: 'NEW',
    });
    if (!load) {
      return response.status(400).json(
          new CustomError({
            message: 'No new load with such Id found',
          }),
      );
    }
    load.status = 'POSTED';
    await load.save();
    // await Load.findOneAndUpdate({ _id: load._id }, { status: "POSTED" });

    let neededTypeOfTruck = ''; // filtering types of trucks

    for (const i in truckTypes) {
      const [width, length, height, payload] = truckTypes[i];
      if (
        load.payload <= payload &&
        ((load.dimensions.width <= width &&
          load.dimensions.length <= length &&
          load.dimensions.height <= height) || // width changes with length
          (load.dimensions.width <= length &&
            load.dimensions.length <= width &&
            load.dimensions.height <= height))
      ) {
        neededTypeOfTruck = i;
        break;
      }
    }

    if (!neededTypeOfTruck) {
      load.logs.push({
        message: 'No compatible truck found',
        time: new Date(),
      });
      load.status = 'NEW';
      await load.save();
      return response.status(400).json(
          new CustomError({
            message: 'No compatible truck found',
          }),
      );
    }
    console.log(neededTypeOfTruck);
    const truck = await Truck.findOne({
      status: 'IS',
      $or: [
        {type: neededTypeOfTruck},
        {type: neededTypeOfTruck === 'SPRINTER' ? 'SMALL STRAIGHT' : ''},
        {
          type:
            neededTypeOfTruck === 'SPRINTER' ||
            neededTypeOfTruck === 'SMALL STRAIGHT' ?
              'LARGE STRAIGHT' :
              '',
        },
      ],
    });

    if (!truck) {
      load.logs.push({
        message: 'No available truck found',
        time: new Date(),
      });
      load.status = 'NEW';
      await load.save();
      return response.status(400).json(
          new CustomError({
            message: 'No available truck found',
          }),
      );
    }

    load.logs.push({
      message: `Load was assigned to driver ${truck.assigned_to}`,
      time: new Date(),
    });
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.assigned_to = truck.assigned_to;

    truck.status = 'OL';

    await load.save();
    await truck.save();

    return response.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    response.status(500).json(new CustomError({message: err.message}));
  }
};

module.exports.getShipperActiveLoads = (request, response) => {
  const id = request.params.id;
  if (request.user.role !== 'SHIPPER') {
    return response.status(400).json(
        new CustomError({
          message: 'Only shipper can view active loads.',
        }),
    );
  }

  Load.find({
    _id: id,
    created_by: request.user._id,
    $or: [{status: 'ASSIGNED'}, {status: 'SHIPPED'}],
  })
      .exec()
      .then((loadsList) => {
        if (!loadsList) {
          return response.status(400).json(
              new CustomError({
                message: 'No active shipper loads with such Id were found',
              }),
          );
        }
        response.status(200).json({loads: loadsList});
      })
      .catch((err) => {
        response.status(500).json(new CustomError({message: err.message}));
      });
};
