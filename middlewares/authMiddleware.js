// const auth = require('../config/auth');

const jwt = require('jsonwebtoken');

const {secret} = require('../config/auth');
const CustomError = require('../models/error');

module.exports = (request, response, next) => {
  const authHeader = request.headers['authorization'];

  if (!authHeader) {
    return response
        .status(401)
        .json(new CustomError({message: 'No authorization header found'}));
  }

  try {
    request.user = jwt.verify(authHeader, secret);
    next();
  } catch (err) {
    return response.status(401).json(new CustomError({message: 'Invalid JWT'}));
  }
};
