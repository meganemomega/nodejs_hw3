/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {
  getOwnUser,
  deleteOwnUser,
  changePasswordOfOwnUser,
} = require('../controllers/userController');

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/users/me', authMiddleware, getOwnUser);
router.delete('/users/me', authMiddleware, deleteOwnUser);
router.patch('/users/me/password', authMiddleware, changePasswordOfOwnUser);

module.exports = router;
