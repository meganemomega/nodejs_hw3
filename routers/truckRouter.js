/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});

const bodySchema = Joi.object({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
});

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckController');

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/trucks', authMiddleware, getTrucks);
router.post('/trucks', authMiddleware, validator.body(bodySchema), addTruck);
router.get('/trucks/:id', authMiddleware, getTruckById);
router.put('/trucks/:id', authMiddleware, updateTruckById);
router.delete('/trucks/:id', authMiddleware, deleteTruckById);
router.post('/trucks/:id/assign', authMiddleware, assignTruckById);

module.exports = router;
