/* eslint-disable require-jsdoc */
/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});

const bodySchemaRegister = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
});

const bodySchemaLogin = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

const {register, login} = require('../controllers/authController');

router.post('/auth/register', validator.body(bodySchemaRegister), register);
router.post('/auth/login', validator.body(bodySchemaLogin), login);

module.exports = router;
