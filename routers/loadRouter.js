/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validator = require('express-joi-validation').createValidator({});

const bodySchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().required(),
    height: Joi.number().required(),
    length: Joi.number().required(),
  },
});

const {
  getLoads,
  addLoad,
  getDriverActiveLoad,
  iterateStateOfLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShipperActiveLoads,
} = require('../controllers/loadController');

const authMiddleware = require('../middlewares/authMiddleware');

router.get('/loads', authMiddleware, getLoads);
router.post('/loads', authMiddleware, validator.body(bodySchema), addLoad);
router.get('/loads/active', authMiddleware, getDriverActiveLoad);
router.patch('/loads/active/state', authMiddleware, iterateStateOfLoad);
router.get('/loads/:id', authMiddleware, getLoadById);
router.put('/loads/:id', authMiddleware, updateLoadById);
router.delete('/loads/:id', authMiddleware, deleteLoadById);
router.post('/loads/:id/post', authMiddleware, postLoadById);
router.get('/loads/:id/shipping_info', authMiddleware, getShipperActiveLoads);

module.exports = router;
