const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan'); // to log requests
const fs = require('fs');
const path = require('path');
const app = express();

const {port} = require('./config/server');
const dbConfig = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

mongoose.connect(
    `mongodb+srv://${dbConfig.username}:${dbConfig.password}@${dbConfig.cluster}/${dbConfig.dbName}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    },
);

app.use(express.json());

const accessLogStream = fs
    .createWriteStream(path.join(__dirname, 'logs.txt'), {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

app.listen(port, () => {
  console.log(`Server listens on ${port} port`);
});
