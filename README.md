# nodejs_hw3
`npm start`
`npm run lint`
`npm run lintfix`

**Acceptance criteria:**
- Driver is able to register in the system;
- Driver is able to login into the system;
- Driver is able to view his profile info;
- Driver is able to change his account password;
- Driver is able to add trucks;
- Driver is able to view created trucks;
- Driver is able to assign truck to himself;
- Driver is able to update not assigned to him trucks info;
- Driver is able to delete not assigned to him trucks;
- Driver is able to view assigned to him load;
- Driver is able to interact with assigned to him load;
- Shipper is able to register in the system;
- Shipper is able to login into the system;
- Shipper is able to view his profile info;
- Shipper is able to change his account password;
- Shipper is able to delete his account;
- Shipper is able to create loads in the system;
- Shipper is able to view created loads;
- Shipper is able to update loads with status ‘NEW';
- Shipper is able to delete loads with status 'NEW';
- Shipper is able to post a load;
- Shipper is able to view shipping info;
- API documentation uploaded to repository;
- README with basic information on how to set up and launch your project should be included ( also what external software should be installed to run your app );
- Project logic distributed across different directories and files in simple and easy-to-understand structure, MVC pattern will be a plus;
- Source code uploaded to repository and link sent to google docs document with all homeworks.


**Optional criteria**
- Any system user can easily reset his password using 'forgot password' option;
- User is able to attach photo to his profile;
- Any system user can see weather information which should be stored on server side;
- User can generate reports about shipped loads, in excel or pdf formats and download them;
- Ability to filter loads by status;
- Pagination for loads;
- [UI] User can interact with application through simple UI application(choose comfortable for you framework or use native js);
- [UI] Shipper is able to see his load info(pick-up address, delivery address), and [UI] driver assigned to his load coordinates on the map on UI;
- [UI] Driver is able to see info about assigned to him load(pick-up address, delivery address) on the map on UI;
- [UI] Any system user is able to interact with the system UI using a mobile phone without any issues;

**🤘🎸Rockstar criteria**
- Driver and Shipper can contact each other through simple chat related to load;
- Driver and Shipper can receive real time shipments  updates through WS;
- The most important functionality covered with unit and acceptance tests;
- [UI] Ability for any system user to choose language on UI(localization);
- Application can handle time zones difference and notify driver if needed;
- Any system user can get notifications through the email about shipment updates;


**Requirements:**
- Use express to implement web-server;
- Use express Router for scaling your app;
- Use JWT as strategy for authentication;
- Use mongoose as ODM for MongoDB;
- Use promises(native or async-await) syntax in all project;
- Use eslint code linter with default google rules to validate your code;
- Use Joi to validate all incoming requests structure;
- All incoming requests should be logged;
- All operations such as posting a load, assigning load to driver etc. should be logged;
- Git repo should  not contain secrets or other sensitive info, use predefined defaults instead.
- All errors should be handled by server and appropriate status and error message should be sent in response to client;
- Use JSON as format to transfer data to and receive data from client;
- Use JSON template for all responses(described in API doc);
- Send response to client only when server finished to process client request and all needed operations finished;
- Use config npm module for configuration, anyone should be able to run your project with config defined in config file or environment variable;
- All users passwords should be stored as encrypted in database.

